pub struct ApplicationTimeOut {
    system_tick: u32,
    system_max_tick: u32,
}

impl ApplicationTimeOut {
        fn system_clock (&mut self) {
            use std::process;


            while self.system_tick != self.system_max_tick {
                use std::time::Duration;
                use std::thread;

                let one_second = Duration::from_secs(1);
                thread::sleep(one_second);
                    self.system_tick +=1;
            } 
        process::exit(0x000);

        }   
}


pub fn start_system_clock () {
    let mut clock = ApplicationTimeOut {
        system_tick:  0, 
        system_max_tick: 86400,
    };

    clock.system_clock();
}

