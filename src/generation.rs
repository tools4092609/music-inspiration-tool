
use rand::Rng;
use crate::diatonic_key_structures;

fn print_scale(music_key: &diatonic_key_structures::DiatonicKeyStruct) {
    println!("Key Tonic: {}", music_key.key_tonic);
    println!("Availiable Notes: {}", music_key.available_notes);
    println!("Structure(Descend): {}", music_key.available_notes_down);
    println!("Structure: {}", music_key.interval_structure);
}

pub fn generate_random() {
    let tempo: u8 = rand::thread_rng().gen_range(60..=180); 
    let key_tonic: u8 = rand::thread_rng().gen_range(1..=14);
    let mode: u8 = rand::thread_rng().gen_range(1..=6);
    let time_signature: u8 = rand::thread_rng().gen_range(1..=4);
    
    match mode { 
        1 => { // Major / Ionian
            println!("Mode: Major / Ionian");
            match key_tonic {
                1 => {print_scale(&diatonic_key_structures::C_MAJOR);}
                2 => {print_scale(&diatonic_key_structures::G_MAJOR);}
                3 => {print_scale(&diatonic_key_structures::D_MAJOR);}
                4 => {print_scale(&diatonic_key_structures::A_MAJOR);}
                5 => {print_scale(&diatonic_key_structures::E_MAJOR);}
                6 => {print_scale(&diatonic_key_structures::B_MAJOR);}
                7 => {print_scale(&diatonic_key_structures::FS_MAJOR);}
                8 => {print_scale(&diatonic_key_structures::CS_MAJOR);}
                9 => {print_scale(&diatonic_key_structures::F_MAJOR);}
                10 => {print_scale(&diatonic_key_structures::BB_MAJOR);}
                11 => {print_scale(&diatonic_key_structures::EB_MAJOR);}
                12 => {print_scale(&diatonic_key_structures::AB_MAJOR);}
                13 => {print_scale(&diatonic_key_structures::DB_MAJOR);}
                14 => {print_scale(&diatonic_key_structures::GB_MAJOR);}
                _ => {
                   println!("Invalid"); 
                }
            }
        }
        2 => { // Minor / Aeolion
            println!("Mode: Minor / Aeolian");
            match key_tonic {
                1 => {print_scale(&diatonic_key_structures::A_MINOR);}
                2 => {print_scale(&diatonic_key_structures::E_MINOR);}
                3 => {print_scale(&diatonic_key_structures::B_MINOR);}
                4 => {print_scale(&diatonic_key_structures::FS_MINOR);}
                5 => {print_scale(&diatonic_key_structures::CS_MINOR);}
                6 => {print_scale(&diatonic_key_structures::GS_MINOR);}
                7 => {print_scale(&diatonic_key_structures::DS_MINOR);}
                8 => {print_scale(&diatonic_key_structures::AS_MINOR);}
                9 => {print_scale(&diatonic_key_structures::AB_MINOR);}
                10 => {print_scale(&diatonic_key_structures::EB_MINOR);}
                11 => {print_scale(&diatonic_key_structures::BB_MINOR);}
                12 => {print_scale(&diatonic_key_structures::F_MINOR);}
                13 => {print_scale(&diatonic_key_structures::G_MINOR);}
                14 => {print_scale(&diatonic_key_structures::D_MINOR);}
                _ => {
                   println!("Invalid"); 
                }
            }
        }
        3 => { // Harmonic Minor
            println!("Mode: Harmonic Minor");
                match key_tonic {
                1 => {print_scale(&diatonic_key_structures::A_HMINOR);}
                2 => {print_scale(&diatonic_key_structures::E_HMINOR);}
                3 => {print_scale(&diatonic_key_structures::B_HMINOR);}
                4 => {print_scale(&diatonic_key_structures::FS_HMINOR);}
                5 => {print_scale(&diatonic_key_structures::CS_HMINOR);}
                6 => {print_scale(&diatonic_key_structures::GS_HMINOR);}
                7 => {print_scale(&diatonic_key_structures::DS_HMINOR);}
                8 => {print_scale(&diatonic_key_structures::AS_HMINOR);}
                9 => {print_scale(&diatonic_key_structures::AB_HMINOR);}
                10 => {print_scale(&diatonic_key_structures::EB_HMINOR);}
                11 => {print_scale(&diatonic_key_structures::BB_HMINOR);}
                12 => {print_scale(&diatonic_key_structures::F_HMINOR);}
                13 => {print_scale(&diatonic_key_structures::G_HMINOR);}
                14 => {print_scale(&diatonic_key_structures::D_HMINOR);}
                _ => {
                   println!("Invalid"); 
                }

                        }
        }
        4 => { // Melodic Minor
            println!("Mode: Melodic Minor");
            match key_tonic {
                1 => {print_scale(&diatonic_key_structures::A_MMINOR);}
                2 => {print_scale(&diatonic_key_structures::E_MMINOR);}
                3 => {print_scale(&diatonic_key_structures::B_MMINOR);}
                4 => {print_scale(&diatonic_key_structures::FS_MMINOR);}
                5 => {print_scale(&diatonic_key_structures::CS_MMINOR);}
                6 => {print_scale(&diatonic_key_structures::GS_MMINOR);}
                7 => {print_scale(&diatonic_key_structures::DS_MMINOR);}
                8 => {print_scale(&diatonic_key_structures::AS_MMINOR);}
                9 => {print_scale(&diatonic_key_structures::AB_MMINOR);}
                10 => {print_scale(&diatonic_key_structures::EB_MMINOR);}
                11 => {print_scale(&diatonic_key_structures::BB_MMINOR);}
                12 => {print_scale(&diatonic_key_structures::F_MMINOR);}
                13 => {print_scale(&diatonic_key_structures::G_MMINOR);}
                14 => {print_scale(&diatonic_key_structures::D_MMINOR);}
                _ => {println!("Invalid");}
            }
            
        }
        5 => { // Blues
            println!("Mode: Heptatonic Blues");
            match key_tonic {
                1 => {print_scale(&diatonic_key_structures::C_BLUESH);}
                2 => {print_scale(&diatonic_key_structures::G_BLUESH);}
                3 => {print_scale(&diatonic_key_structures::D_BLUESH);}
                4 => {print_scale(&diatonic_key_structures::A_BLUESH);}
                5 => {print_scale(&diatonic_key_structures::E_BLUESH);}
                6 => {print_scale(&diatonic_key_structures::B_BLUESH);}
                7 => {print_scale(&diatonic_key_structures::FS_BLUESH);}
                8 => {print_scale(&diatonic_key_structures::CS_BLUESH);}
                9 => {print_scale(&diatonic_key_structures::F_BLUESH);}
                10 => {print_scale(&diatonic_key_structures::BB_BLUESH);}
                11 => {print_scale(&diatonic_key_structures::EB_BLUESH);}
                12 => {print_scale(&diatonic_key_structures::AB_BLUESH);}
                13 => {print_scale(&diatonic_key_structures::DB_BLUESH);}
                14 => {print_scale(&diatonic_key_structures::GB_BLUESH);}
                _ => {println!("Invalid");}
            }
        }
        6 => { // Pentatonic
             println!("Mode: Pentatonic Major");
             match key_tonic {
                1 => {print_scale(&diatonic_key_structures::C_MAJORP);}
                2 => {print_scale(&diatonic_key_structures::G_MAJORP);}
                3 => {print_scale(&diatonic_key_structures::D_MAJORP);}
                4 => {print_scale(&diatonic_key_structures::A_MAJORP);}
                5 => {print_scale(&diatonic_key_structures::E_MAJORP);}
                6 => {print_scale(&diatonic_key_structures::B_MAJORP);}
                7 => {print_scale(&diatonic_key_structures::FS_MAJORP);}
                8 => {print_scale(&diatonic_key_structures::CS_MAJORP);}
                9 => {print_scale(&diatonic_key_structures::F_MAJORP);}
                10 => {print_scale(&diatonic_key_structures::BB_MAJORP);}
                11 => {print_scale(&diatonic_key_structures::EB_MAJORP);}
                12 => {print_scale(&diatonic_key_structures::AB_MAJORP);}
                13 => {print_scale(&diatonic_key_structures::DB_MAJORP);}
                14 => {print_scale(&diatonic_key_structures::GB_MAJORP);}
                _ => {
                   println!("Invalid"); 
                }

  
            }
        }
        _ => {

        }

    }
    // Tempo
    println!("Tempo: {tempo}");
    // Time Signature
    match time_signature {
        1 => println!("Time Signature: 4/4\n"),
        2 => println!("Time Signature: 3/4\n"),
        3 => println!("Time Signature: 5/4\n"),
        4 => println!("Time Signature: 6/8\n"),
        // 5 => println!("Time Signature: 2/4\n"),
        _ => println!("Time Signature: Invalid\n"),
    }
    }

