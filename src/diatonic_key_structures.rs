pub struct DiatonicKeyStruct {
    pub key_tonic: &'static str,
    pub available_notes: &'static str,
    pub interval_structure: &'static str,
    pub available_notes_down: &'static str,
}

// Structs are read only and will be printed in the print fucntion
// Major Keys

pub const C_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "C",
    available_notes: "C,D,E,F,G,A,B,C",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const G_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "G",
    available_notes: "G,A,B,C,D,E,F#,G",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const D_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "D",
    available_notes: "D,E,F#,G,A,B,C,D",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const A_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "A",
    available_notes: "A,B,C#,D,E,F#,G#,A",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const E_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "E",
    available_notes: "E,F#,G#,A,B,C#,D#,E",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const B_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "B",
    available_notes: "B,C#,D#,E,F#,G#,A#,B",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const FS_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "F#",
    available_notes: "F#,G#,A#,B,C#,D#,E#,F#",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const CS_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "C#",
    available_notes: "C#,D#,E#,F#,G#,A#,B#,C#",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const F_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "F",
    available_notes: "F,G,A,Bb,C,D,E,F",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const BB_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Bb",
    available_notes: "Bb,C,D,Eb,F,G,A,Bb",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const EB_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Eb",
    available_notes: "Eb,F,G,Ab,Bb,C,D,Eb",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const AB_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Ab",
    available_notes: "Ab,Bb,C,Db,Eb,F,G,Ab",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const GB_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Gb",
    available_notes: "Gb,Ab,Bb,Cb,Db,Eb,F,Gb",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const DB_MAJOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Db",
    available_notes: "Db,Eb,F,Gb,Ab,Bb,C,Db",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

// MINOR Natural

pub const A_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "A",
    available_notes: "A,B,C,D,E,F,G,A",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const E_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "E",
    available_notes: "E.F#,G,A,B,C,D,E",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const B_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "B",
    available_notes: "B,C#,D,E,F#,G,A,B",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const FS_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "F#",
    available_notes: "F#,G#,A,B,C#,D,E,F#",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const CS_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "C#",
    available_notes: "C#,D#,E,F#,G#,A,B,C#",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const GS_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "G#",
    available_notes: "G#,A#,B,C#,D#,E,F#,G#",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const DS_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "D#",
    available_notes: "D#,E#,F#,G#,A#,B,C#,D#",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const AS_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "A#",
    available_notes: "A#,B#,C#,D#,E#,F#,G#,A#",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const AB_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Ab",
    available_notes: "Ab,Bb,Cb,Db,Eb,Fb,Gb,Ab",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const EB_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Eb",
    available_notes: "Eb,F,GB,Ab,Bb,Cb,Db,Eb",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const BB_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Bb",
    available_notes: "Bb,C,Db,Eb,F,Gb,Ab,Bb",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const F_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "F",
    available_notes: "F,G,Ab,Bb,C,Db,Eb,F",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const G_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "G",
    available_notes: "G,A,Bb,C,D,Eb,F,G",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const D_MINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "D",
    available_notes: "D,E,F,G,A,Bb,C,D",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

// MINOR Harmonic

pub const A_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "A",
    available_notes: "A,B,C,D,E,F,G#,A",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Aug+",
};

pub const E_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "E",
    available_notes: "E.F#,G,A,B,C,D#,E",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Aug+",
};

pub const B_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "B",
    available_notes: "B,C#,D,E,F#,G,A#,B",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const FS_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "F#",
    available_notes: "F#,G#,A,B,C#,D,E#,F#",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const CS_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "C#",
    available_notes: "C#,D#,E,F#,G#,A,B#,C#",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const GS_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "G#",
    available_notes: "G#,A#,B,C#,D#,E,G,G#",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const DS_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "D#",
    available_notes: "D#,E#,F#,G#,A#,B,D,D#",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const AS_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "A#",
    available_notes: "A#,B#,C#,D#,E#,F#,A,A#",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const AB_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Ab",
    available_notes: "Ab,Bb,Cb,Db,Eb,Fb,G,Ab",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const EB_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Eb",
    available_notes: "Eb,F,GB,Ab,Bb,Cb,D,Eb",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const BB_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Bb",
    available_notes: "Bb,C,Db,Eb,F,Gb,A,Bb",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const F_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "F",
    available_notes: "F,G,Ab,Bb,C,Db,E,F",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const G_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "G",
    available_notes: "G,A,Bb,C,D,Eb,F#,G",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

pub const D_HMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "D",
    available_notes: "D,E,F,G,A,Bb,C#,D",
    available_notes_down: "N/A",
    interval_structure: "min,dim,Maj,min,min,Maj,Maj",
};

// Melodic Minor

pub const A_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "A",
    available_notes: "A,B,C,D,E,F#,G#,A",
    available_notes_down: "A,B,C,D,E,F,G,A",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

pub const E_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "E",
    available_notes: "E,F#,G,A,B#,C#,D",
    available_notes_down: "E.F#,G,A,B,C,D,",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

pub const B_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "B",
    available_notes: "B,C#,D,E,F#,G#,A#,B",
    available_notes_down: "B,C#,D,E,F#,G,A,B",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

pub const FS_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "F#",
    available_notes: "F#,G#,A,B,C#,D#,E#,F#",
    available_notes_down: "F#,G#,A,B,C#,D,E,F#",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

pub const CS_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "C#",
    available_notes: "C#,D#,E,F#,G#,A#,B#,C#",
    available_notes_down: "C#,D#,E,F#,G#,A,B,C#",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

pub const GS_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "G#",
    available_notes: "G#,A#,B,C#,D#,E#,G,G#",
    available_notes_down: "G#,A#,B,C#,D#,E,F#,G#",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

pub const DS_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "D#",
    available_notes: "D#,E#,F#,G#,A#,C,D,D#",
    available_notes_down: "D#,E#,F#,G#,A#,B,C#,D#",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

pub const AS_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "A#",
    available_notes: "A#,B#,C#,D#,E#,G,A,A#",
    available_notes_down: "A#,B#,C#,D#,E#,F#,G#,A#",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

pub const AB_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Ab",
    available_notes: "Ab,Bb,Cb,Db,Eb,F,G,Ab",
    available_notes_down: "Ab,Bb,Cb,Db,Eb,Fb,Gb,Ab",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

pub const EB_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Eb",
    available_notes: "Eb,F,Gb,Ab,Bb,C,D,Eb",
    available_notes_down: "Eb,F,Gb,Ab,Bb,Cb,Db,Eb",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

pub const BB_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Bb",
    available_notes: "Bb,C,Db,Eb,F,G,A,Bb",
    available_notes_down: "Bb,C,Db,Eb,F,Gb,Ab,Bb",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

pub const F_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "F",
    available_notes: "F,G,Ab,Bb,C,D,E,F",
    available_notes_down: "F,G,Ab,Bb,C,Db,Eb,F",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

pub const G_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "G",
    available_notes: "G,A,Bb,C,D,E,Gb,G",
    available_notes_down: "G,A,Bb,C,D,Eb,F,G",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

pub const D_MMINOR: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "D",
    available_notes: "D,E,F,G,A,B,Db,D",
    available_notes_down: "D,E,F,G,A,Bb,C,D",
    interval_structure: "(Up) min,dim,Maj,min,min,Maj,Maj",
};

// Major Pentatonic
pub const C_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "C",
    available_notes: "C,D,E,G,A,C",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const G_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "G",
    available_notes: "G,A,B,D,E,G",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const D_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "D",
    available_notes: "D,E,F#,A,B,D",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const A_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "A",
    available_notes: "A,B,C#,E,F#,A",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const E_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "E",
    available_notes: "E,F#,G#,B,C#,E",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const B_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "B",
    available_notes: "B,C#,D#,F#,G#,B",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const FS_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "F#",
    available_notes: "F#,G#,A#,C#,D#,F#",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const CS_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "C#",
    available_notes: "C#,D#,E#,G#,A#,C#",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const F_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "F",
    available_notes: "F,G,A,C,D,F",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const BB_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Bb",
    available_notes: "Bb,C,D,F,G,Bb",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const EB_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Eb",
    available_notes: "Eb,F,G,Bb,C,Eb",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const AB_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Ab",
    available_notes: "Ab,Bb,C,Eb,F,Ab",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const GB_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Gb",
    available_notes: "Gb,Ab,Bb,Db,Eb,Gb",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

pub const DB_MAJORP: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Db",
    available_notes: "Db,Eb,F,Ab,Bb,Db",
    available_notes_down: "N/A",
    interval_structure: "Maj,min,min,Maj,Maj,min,dim",
};

// Blues Scale
pub const C_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "C",
    available_notes: "C,D,Eb,F,Gb,A,Bb,C",
    available_notes_down: "N/A",
    interval_structure: "N/A",
};

pub const G_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "G",
    available_notes: "G,A,Bb,C,Db,E,F,G",
    available_notes_down: "N/A",
    interval_structure: "N/A",
};

pub const D_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "D",
    available_notes: "D,E,F,G,Ab,B,Cb,D",
    available_notes_down: "N/A",
    interval_structure: "N/A"
};

pub const A_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "A",
    available_notes: "A,B,C,D,Eb,F#,G,A",
    available_notes_down: "N/A",
    interval_structure: "N/A",
};

pub const E_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "E",
    available_notes: "E,F#,G,A,Bb,C#,D,E",
    available_notes_down: "N/A",
    interval_structure: "N/A",
};

pub const B_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "B",
    available_notes: "B,C#,D,E,F,G#,A,B",
    available_notes_down: "N/A",
    interval_structure: "N/A",
};

pub const FS_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "F#",
    available_notes: "F#,G#,A,B,C,D#,E,F#",
    available_notes_down: "N/A",
    interval_structure: "N/A",
};

pub const CS_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "C#",
    available_notes: "C#,D#,E,F#,G,A#,B,C#",
    available_notes_down: "N/A",
    interval_structure: "N/A",
};

pub const F_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "F",
    available_notes: "F,G,Ab,Bb,Cb,D,Eb,F",
    available_notes_down: "N/A",
    interval_structure: "N/A",
};

pub const BB_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Bb",
    available_notes: "Bb,C,Db,Eb,Fb,G,Ab,Bb",
    available_notes_down: "N/A",
    interval_structure: "N/A",
};

pub const EB_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Eb",
    available_notes: "Eb,F,Gb,Ab,A,C,Db,Eb",
    available_notes_down: "N/A",
    interval_structure: "N/A",
};

pub const AB_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Ab",
    available_notes: "Ab,Bb,Cb,Db,D,F,Gb,Ab",
    available_notes_down: "N/A",
    interval_structure: "N/A",
};

pub const GB_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Gb",
    available_notes: "Gb,Ab,A,Cb,C,Eb,Fb,Gb",
    available_notes_down: "N/A",
    interval_structure: "N/A",
};

pub const DB_BLUESH: DiatonicKeyStruct = DiatonicKeyStruct {
    key_tonic: "Db",
    available_notes: "Db,Eb,Fb,Gb,G,Bb,Cb,Db",
    available_notes_down: "N/A",
    interval_structure: "N/A",
};

