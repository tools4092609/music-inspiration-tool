mod generation;
mod diatonic_key_structures;
mod system;

fn main() {

    let mut repeat_counter = 3;
    println!("Try to compose a piece of music with these parameters...");

    while repeat_counter != 0{
        generation::generate_random();
        repeat_counter -=1;
    }
    system::start_system_clock();
}


